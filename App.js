import { StatusBar } from 'expo-status-bar';
import {StyleSheet, Text, View, ActivityIndicator, Image, TouchableOpacity, FlatList, Dimensions} from 'react-native';
import * as Location from 'expo-location';
import React, {useState, useEffect} from "react";
import {ImageBackground} from "react-native-web";
import MapView, {Marker} from 'react-native-maps';

export default function App() {
  const [location, setLocation] = useState(null);
  const [error, setError] = useState(null);
  const [city, setCity] = useState([]);
  const [temp, setTemp] = useState([]);
  const [description, setDescription] = useState([]);
  const [icon, setIcon] = useState([]);
  const [weatherdays, setWeatherday] = useState([]);
  const [lat, setlat] =useState([])
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null)



  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setError('Permission non accordé');
        return;
      }
      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    }) ()
  }, []);


  let key = '416cbbdb2782bf4538b35f25165d305f';



  const getdaily = () => {
    fetch(`https://api.openweathermap.org/data/2.5/onecall?units=metric&lang=fr&lat=${location.coords.latitude}&lon=${location.coords.longitude}&appid=${key}`)
        .then(function(response){
          return response.json();
        }).then(function(response){
          setWeatherday(response.daily)
          console.log(response.daily)
    })
  }

  const getmeteo = () => {
    fetch(`https://api.openweathermap.org/data/2.5/weather?units=metric&lang=fr&lat=${location.coords.latitude}&lon=${location.coords.longitude}&appid=${key}`)
    .then(function(response){
      return response.json();
    }).then(function(response){
      setTemp(response.main.temp);
      setCity(response.name);
      setDescription(response.weather[0].description);
      setIcon(response.weather[0].icon);
      setLatitude(response.coord.lat);
      setLongitude(response.coord.lon)
    })
  }


  useEffect(() => {
    location && getmeteo();
    location && getdaily();
  },  [location])



  return (
      <View style={description == 'ciel dégagé' ? styles.fullcielclean: description == 'peu nuageux' ? styles.fewclouds:
          description == 'nuageux' ? styles.scattered: description == 'couvert' ? styles.brokenclouds:
              description == 'pluie modérée' ? styles.showerrain: description == 'légère pluie' ? styles.rain: description == 'orages' ? styles.thunderstorm:
                  description == 'neige' ? styles.snow: description == 'brouillard' ? styles.mist: styles.basic}>
        <View style={styles.container}>
          {location == null ? <ActivityIndicator size="large"/>: null}
          <View style={styles.principal}>
            <View style={styles.row}>
              <Text style={description == 'orages' ? styles.citythunderstorm: styles.city}>{city}</Text>
              <Image style={styles.image} source={{
                uri:`http://openweathermap.org/img/w/${icon}.png`
              }}/>
            </View>
            <Text style={description == 'orages' ? styles.citythunderstorm: styles.temp}>{temp}{location != null ? ' °C': null}</Text>
            <Text style={description == 'orages' ? styles.citythunderstorm: styles.describe}>{description}</Text>
          </View>
          <View>
            {location != null ? <TouchableOpacity onPress={getmeteo} style={styles.actualiser} ><Text style={styles.act}>Actualiser</Text></TouchableOpacity>: null}
          </View>

        </View>
        <View style={styles.container2}>
          <FlatList horizontal={true} data={weatherdays} renderItem={({item}) => (<View style={styles.cards}>
            <View style={item.weather[0].description == 'ciel dégagé' ? styles.fullcielclean: item.weather[0].description == 'partiellement nuageux' ? styles.fewclouds:
                item.weather[0].description == 'nuageux' ? styles.scattered: item.weather[0].description == 'couvert' ? styles.brokenclouds:
                    item.weather[0].description == 'pluie modérée' ? styles.showerrain: item.weather[0].description == 'légère pluie' ? styles.rain: item.weather[0].description == 'orages' ? styles.thunderstorm:
                        item.weather[0].description == 'neige' ? styles.snow: item.weather[0].description == 'brouillard' ? styles.mist: styles.basic}>
              <View style={styles.padd}>
                <Text style={item.weather[0].description == 'orages' ? styles.weatherthundertemp: styles.tempday}>{item.temp.day}{weatherdays != null ? ' °C': null}</Text>
                <Text style={item.weather[0].description == 'orages' ? styles.weatherthunderdesc: styles.description}>{item.weather[0].description}</Text>
                <Text style={item.weather[0].description == 'orages' ? styles.weatherthunderhum: styles.humidity}>{item.humidity}{weatherdays != null ? " %": null}</Text>
              </View>
            </View>
          </View>)} />
        </View>
        <View style={styles.container3}>
          {latitude != null && longitude != null ? <MapView style={styles.map} region={{
            latitude: latitude,
            longitude: longitude,
            latitudeDelta: 0.1,
            longitudeDelta: 0.1
          }}>
            <MapView.Marker coordinate={{
              latitude: latitude,
              longitude: longitude
            }} title={'My marker'} description={'Marker'}/>
          </MapView>: null}
        </View>
        <StatusBar style="auto" />
      </View>
  );
}

const styles = StyleSheet.create({
  container3: {
    height: '25%',
  },
  map: {
    height: '100%',
  },
  weatherthundertemp: {
    fontSize: 20,
    lineHeight: 50,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white'
  },
  weatherthunderdesc:{
    lineHeight: 25,
    fontSize: 15,
    textAlign: 'center',
    color: 'white',
  },
  weatherthunderhum: {
    textAlign: "center",
    color: 'white',
  },
  padd: {
    padding: 10
  },
  humidity: {
    textAlign: "center"
  },
  tempday: {
    fontSize: 20,
    lineHeight: 50,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  description: {
    lineHeight: 25,
    fontSize: 15,
    textAlign: 'center'
  },
  cards: {
    backgroundColor: 'white',
    height: '60%',
    borderRadius: 10,
    marginHorizontal: 7,

  },
  fullcielclean: {
    flex: 1,
    backgroundColor: '#ffd700'
  },
  container2: {
    height: '25%',
  },
  citythunderstorm: {
    fontSize: 30,
    color: 'white'
  },
  scattered: {
    flex: 1,
    backgroundColor: '#d3d3d3'
  },
  showerrain: {
    flex: 1,
    backgroundColor: '#b0e0e6'
  },
  rain: {
    flex: 1,
    backgroundColor: '#1e90ff'
  },
  thunderstorm: {
    flex: 1,
    backgroundColor: '#00008b'
  },
  snow: {
    flex: 1,
    backgroundColor: '#f0ffff'
  },
  mist: {
    flex: 1,
    backgroundColor: '#dcdcdc'
  },
  brokenclouds: {
    flex: 1,
    backgroundColor: '#c0c0c0'
  },
  fewclouds: {
    flex: 1,
    backgroundColor: '#eee8aa'
  },
  basic: {
    flex: 1,
    backgroundColor: '#fff'
  },
  city: {
    fontSize: 30,
  },
  actualiser: {
    backgroundColor: 'grey',
    padding: 10,
    borderRadius: 4,
  },
  act: {
    color: 'white'
  },
  describe: {
    fontSize: 30,
    padding: 5
  },
  temp: {
    fontSize: 50,
    fontWeight: 'bold'
  },
  principal: {
    paddingLeft: '20%',
    paddingRight: '20%',
    paddingTop: '5%',
    paddingBottom: '5%'
  },
  row: {
    flexDirection: 'row'
  },
  image: {
    width: 50,
    height: 50,
  },
  container: {
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
